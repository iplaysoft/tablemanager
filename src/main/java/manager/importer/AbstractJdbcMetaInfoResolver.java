package manager.importer;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

import javax.sql.DataSource;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public abstract class AbstractJdbcMetaInfoResolver implements MetaInfoResolver {

    @Override
    public List<EntityMetaInfo> resolve(Object resouce) {
        JdbcConfig jdbcConfig = (JdbcConfig) resouce;
        Driver driver = null;
        try {
            driver = getDriver();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return Collections.EMPTY_LIST;
        }
        DataSource dataSource = new SimpleDriverDataSource(driver,
                jdbcConfig.getUrl(), jdbcConfig.getUsername(), jdbcConfig.getPassword());
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        java.util.List<Map<String, Object>> mapList = jdbcTemplate.queryForList(getSelectSql());
        return parseSqlResultList(mapList);
    }


    protected abstract Driver getDriver() throws SQLException;

    protected abstract String getSelectSql();

    protected abstract List<EntityMetaInfo> parseSqlResultList(List<Map<String, Object>> resultList);

}
