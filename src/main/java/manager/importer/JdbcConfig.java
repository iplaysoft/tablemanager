package manager.importer;

import lombok.Data;

/**
 * @author yongfeng_meng
 */
@Data
public class JdbcConfig {
    private String url;
    private String username;
    private String password;
}
