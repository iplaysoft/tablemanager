package manager.importer;

import manager.domain.Entity;
import manager.domain.EntityItem;
import manager.service.EntityItemService;
import manager.service.EntityService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author yongfeng_meng
 */
public class EntityImporter {
    @Autowired
    private EntityService entityService;

    @Autowired
    private EntityItemService entityItemService;

    public void doImport(EntityMetaInfo metaInfo) {
        Entity entity = metaInfo.getEntity();
        Long entityId = entityService.create(entity);

        List<EntityItem> entityItems = metaInfo.getEntityItems();
        for (EntityItem entityItem : entityItems) {
            entityItem.setEntityId(entityId);
            entityItemService.create(entityItem);
        }
    }
}
