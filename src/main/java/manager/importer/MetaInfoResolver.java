package manager.importer;

import java.util.List;

/**
 * @author yongfeng_meng
 */
public interface MetaInfoResolver {
    List<EntityMetaInfo> resolve(Object resouce);
}
