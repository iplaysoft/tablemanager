package manager.importer;

import lombok.Data;
import manager.domain.Entity;
import manager.domain.EntityItem;

import java.util.List;

/**
 * @author yongfeng_meng
 */
@Data
public class EntityMetaInfo {
    private Entity entity;
    private List<EntityItem> entityItems;
}
