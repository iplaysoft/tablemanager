package manager.importer;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class MysqlMetaInfoResolver extends AbstractJdbcMetaInfoResolver {

    @Override
    protected Driver getDriver() throws SQLException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return DriverManager.getDriver("com.mysql.cj.jdbc.Driver");
    }

    @Override
    protected String getSelectSql() {
        return "select 1 as a";
    }

    @Override
    protected List<EntityMetaInfo> parseSqlResultList(List<Map<String, Object>> resultList) {
        return null;
    }

    public static void main(String[] args) {
        MysqlMetaInfoResolver mysqlMetaInfoResolver = new MysqlMetaInfoResolver();
        JdbcConfig jdbcConfig = new JdbcConfig();
        jdbcConfig.setUrl("jdbc:mysql://172.16.211.224:3306/platform_subscribe?useSSL=false&useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8");
        jdbcConfig.setUsername("platformsuper");
        jdbcConfig.setPassword("!Sql_Pass@1!");
        mysqlMetaInfoResolver.resolve(jdbcConfig);
    }
}
