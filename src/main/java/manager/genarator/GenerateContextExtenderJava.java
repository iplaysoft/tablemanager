package manager.genarator;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import freemarker.core.JSONOutputFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * @author Tony
 */
@Component("JAVA")
public class GenerateContextExtenderJava implements GenerateContextExtender {

    @Autowired
    private MapperFacade mapperFacade;

    @Override
    public GenerateContext extend(GenerateContext generateContext) {
        GenerateContextJava generateContextJava = mapperFacade.map(generateContext, GenerateContextJava.class);
        ClassInfo classInfo = new ClassInfo();
        classInfo.setClassName(generateContext.getEntity().getName());
        classInfo.setPackageName(parsePackage(generateContext.getTemplateItem().getFileName()));
        generateContextJava.setClassInfo(classInfo);
        return generateContextJava;
    }

    private static final String[] skipPaths = new java.lang.String[]{File.separator, "src", "java"};

    private String parsePackage(String fileName) {
        if (StringUtils.isEmpty(fileName)) {
            return ".";
        }
        String parentPath = new File(fileName).getParent();
        for (int i = 0; i < skipPaths.length;) {
            if (parentPath.startsWith(skipPaths[i])) {
                parentPath = parentPath.substring(skipPaths[i].length());
                i = 0;
            } else {
                i++;
            }
        }
        return parentPath.replace(File.separator, ".");
    }

    @Data
    @NoArgsConstructor
    public static class GenerateContextJava extends GenerateContext {
        private ClassInfo classInfo;
    }

    @Data
    public static class ClassInfo {
        private String packageName;
        private String className;
    }
}