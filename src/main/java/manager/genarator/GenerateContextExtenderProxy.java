package manager.genarator;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Tony
 */
@Data
@Component
public class GenerateContextExtenderProxy {

    @Autowired
    private Map<String, GenerateContextExtender> contextExtenderMap;

    public GenerateContext extend(GenerateContext generateContext) {
        String language = generateContext.getTemplateItem().getLanguage();
        GenerateContextExtender contextExtender = contextExtenderMap.get(language);
        if (contextExtender != null) {
            return contextExtender.extend(generateContext);
        }
        return generateContext;
    }
}