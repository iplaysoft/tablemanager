package manager.genarator;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author Tony
 */
public abstract class AbstractGenerator implements Generator {
    @Autowired
    private GenerateContextExtenderProxy extenderProxy;

    @Override
    public GeneratedResult generate(GenerateContext generateContext) {
        String fileName = parseFileName(generateContext);
        generateContext.getTemplateItem().setFileName(fileName);

        GenerateContext generateContextEx = extenderProxy.extend(generateContext);
        String content = parseContent(generateContextEx);

        return new GeneratedResult(fileName, content, generateContext.getTemplateItem().getLanguage());
    }

    /**
     * @return
     */
    protected abstract String parseFileName(GenerateContext generateContext);

    /**
     * @return
     */
    protected abstract String parseContent(GenerateContext generateContext);
}
