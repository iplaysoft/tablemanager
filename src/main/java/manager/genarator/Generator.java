package manager.genarator;

/**
 * @author Tony
 */
public interface Generator {


    /**
     * 根据模板信息和上下文生成最终内容
     *
     * @param generateContext 上下文
     * @return
     * @throws Exception 异常
     */
    GeneratedResult generate(GenerateContext generateContext);
}
