package manager.genarator;

import lombok.Data;
import manager.domain.*;

import java.util.List;

/**
 * @author Tony
 */
@Data
public class GenerateContext {
    private Project project;
    private Module module;
    private Entity entity;
    private List<EntityItem> entityItems;
    private Template template;
    private TemplateItem templateItem;
}