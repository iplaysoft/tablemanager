package manager.genarator;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import manager.domain.Project;

/**
 * @author Tony
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class GeneratedResult {
    private String fileName;
    private String content;
    private String language;
}