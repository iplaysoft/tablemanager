package manager.genarator;

/**
 * @author Tony
 */
public interface GenerateContextExtender {
    /**
     * 扩展上下文
     *
     * @param generateContext 原上下文
     * @return 扩展后的上下文
     */
    GenerateContext extend(GenerateContext generateContext);
}