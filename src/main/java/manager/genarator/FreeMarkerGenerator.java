package manager.genarator;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import manager.domain.Module;
import manager.domain.TemplateItem;
import manager.util.TimeUtil;
import manager.util.ValueUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.common.TemplateParserContext;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.StringWriter;

/**
 * @author Tony
 */
@Component("FREE_MARKER")
public class FreeMarkerGenerator extends AbstractGenerator {

    @Autowired
    private Configuration configuration;

    @Bean
    public Configuration getConfig() {
        Configuration cfg = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateLoader(new StringTemplateLoader());
        return cfg;
    }

    @Override
    protected String parseFileName(GenerateContext generateContext) {
        StringTemplateLoader templateLoader = (StringTemplateLoader) configuration.getTemplateLoader();
        String fileName = generateContext.getTemplateItem().getFileName();
        if (StringUtils.isNotEmpty(fileName)) {
            String fileNameHash = String.valueOf(fileName.hashCode());
            Object templateSource = templateLoader.findTemplateSource(fileNameHash);
            if (templateSource == null) {
                templateLoader.putTemplate(fileNameHash, fileName);
            }
            try {
                fileName = parse(fileNameHash, generateContext);
            } catch (Exception e) {
            }
        }
        return fileName;
    }

    @Override
    protected String parseContent(GenerateContext generateContext) {
        StringTemplateLoader templateLoader = (StringTemplateLoader) configuration.getTemplateLoader();
        TemplateItem templateItem = generateContext.getTemplateItem();

        // 生成内容
        long templateLastModified = TimeUtil.getTime(ValueUtil.nvl(templateItem.getUpdateTime(), templateItem.getCreateTime()));
        String templateId = String.valueOf(templateItem.getId());
        Object templateSource = templateLoader.findTemplateSource(templateId);
        if (templateSource == null || templateLoader.getLastModified(templateSource) < templateLastModified) {
            templateLoader.putTemplate(String.valueOf(templateId), templateItem.getContent(), templateLastModified);
        }
        String content = null;
        try {
            content = parse(templateId, generateContext);
        } catch (Exception e) {
            content = e.getMessage();
        }
        return content;
    }

    private String parse(String templateId, GenerateContext generateContext) throws IOException, TemplateException {
        StringWriter stringWriter = new StringWriter();
        configuration.getTemplate(templateId).process(generateContext, stringWriter);
        return stringWriter.toString();
    }

    public static void main(String[] args) throws IOException, TemplateException {
        ExpressionParser parser = new SpelExpressionParser();
        Expression expression = parser.parseExpression("#{module.name.toLowerCase()}bc", new TemplateParserContext());
        GenerateContext generateContext = new GenerateContext();
        Module module = new Module();
        module.setName("AV23d");
        generateContext.setModule(module);
        System.out.println(expression.getValue(generateContext, String.class));
    }
}

