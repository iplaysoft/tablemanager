package manager.config;

import ma.glasnost.orika.Converter;
import ma.glasnost.orika.Mapper;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.ConfigurableMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class OrikaMapperConfig {

    @Autowired(required = false)
    private List<Mapper> mappers;

    @Autowired(required = false)
    private List<Converter> converters;

    @Bean
    public MapperFacade getMapperFacade() {
        return new ConfigurableMapper() {
            @Override
            protected void configure(MapperFactory factory) {
                super.configure(factory);
                if (mappers != null) {
                    mappers.forEach(mapper -> factory.classMap(mapper.getAType(), mapper.getBType()).byDefault().customize(mapper).register());
                }
                if (converters != null) {
                    converters.forEach(converter -> factory.getConverterFactory().registerConverter(converter));
                }
            }
        };
    }
}
