package manager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import manager.domain.TemplateItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TemplateItemMapper extends BaseMapper<TemplateItem> {
}
