package manager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import manager.domain.Project;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProjectMapper extends BaseMapper<Project> {
}
