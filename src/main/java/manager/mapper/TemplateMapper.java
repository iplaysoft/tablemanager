package manager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import manager.domain.Template;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TemplateMapper extends BaseMapper<Template> {
}
