package manager.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import manager.domain.EntityItem;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EntityItemMapper extends BaseMapper<EntityItem> {
}
