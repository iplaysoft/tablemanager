package manager.util;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

/**
 * @author Tony
 */
public class FileUtil {
    private static final String Separator = "/";

    public static final String getDirectory(String fileFullPath) {
        if (StringUtils.isEmpty(fileFullPath)) {
            return fileFullPath;
        }
        int lastIndexOfSeparator = fileFullPath.lastIndexOf(Separator);
        if (lastIndexOfSeparator < 0 || lastIndexOfSeparator == fileFullPath.length() - 1) {
            return fileFullPath;
        }

        return fileFullPath.substring(0, fileFullPath.lastIndexOf(Separator) + 1);
    }

    public static final String getFileName(String fileFullPath) {
        if (StringUtils.isEmpty(fileFullPath)) {
            return fileFullPath;
        }
        int lastIndexOfSeparator = fileFullPath.lastIndexOf(Separator);
        if (lastIndexOfSeparator < 0 || lastIndexOfSeparator == fileFullPath.length() - 1) {
            return fileFullPath;
        }
        return fileFullPath.substring(lastIndexOfSeparator + 1);
    }
}
