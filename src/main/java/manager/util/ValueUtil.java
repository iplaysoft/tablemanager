package manager.util;

public class ValueUtil {
    static public <T> T nvl(T val1, T val2) {
        if (val1 != null) {
            return val1;
        }
        return val2;
    }
}
