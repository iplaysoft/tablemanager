package manager.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Tony
 */
public class TimeUtil {
    private static final String defaultDateTimePattern = "yyyy-MM-dd HH:mm:ss";
    private static DateTimeFormatter globalDateTimeFormatter = DateTimeFormatter.ofPattern(defaultDateTimePattern);

    public static String now() {
        LocalDateTime localDateTime = LocalDateTime.now();
        return localDateTime.format(globalDateTimeFormatter);
    }

    public static long getTime(String dateTime) {
        try {
            return new SimpleDateFormat(defaultDateTimePattern).parse(dateTime).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
