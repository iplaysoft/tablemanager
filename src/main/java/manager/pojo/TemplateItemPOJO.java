package manager.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

/**
 * @author Tony
 */
@Data
@TableName("template_item")
public class TemplateItemPOJO {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "template_id")
    private Long templateId;

    @TableField(value = "file_name")
    private String fileName;

    @TableField(value = "name")
    private String name;

    @TableField(value = "manager")
    private String manager;

    @TableField(value = "comment")
    private String comment;

    @TableField(value = "content")
    private String content;

    @TableField(value = "type")
    private String type;

    @TableField(value = "language")
    private String language;

    @TableField(value = "create_time")
    private String createTime;

    @TableField(value = "update_time")
    private String updateTime;

    @TableLogic(value = "del_flg", delval="1")
    private Integer delFlg;
}
