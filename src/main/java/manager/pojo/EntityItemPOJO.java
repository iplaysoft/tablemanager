package manager.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

@Data
@TableName("entity_item")
public class EntityItemPOJO {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "code")
    private String code;

    @TableField(value = "name")
    private String name;

    @TableField(value = "comment")
    private String comment;

    @TableField(value = "entity_id")
    private Long entityId;

    @TableField(value = "length")
    private Integer length;

    @TableField(value = "data_type")
    private String dataType;

    @TableField(value = "can_select")
    private Integer canSelect;

    @TableField(value = "can_update")
    private Integer canUpdate;

    @TableField(value = "can_condition")
    private Integer canCondition;

    @TableField(value = "create_time")
    private String createTime;

    @TableField(value = "update_time")
    private String updateTime;

    @TableLogic(value = "del_flg", delval="1")
    private Integer delFlg;
}
