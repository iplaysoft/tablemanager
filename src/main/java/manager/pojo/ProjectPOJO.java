package manager.pojo;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

/**
 * @author Tony
 */
@Data
@TableName("project")
public class ProjectPOJO {
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @TableField(value = "code")
    private String code;

    @TableField(value = "name")
    private String name;

    @TableField(value = "manager")
    private String manager;

    @TableField(value = "comment")
    private String comment;

    @TableField(value = "create_time")
    private String createTime;

    @TableField(value = "update_time")
    private String updateTime;

    @TableLogic(value = "del_flg", delval="1")
    private Integer delFlg;
}
