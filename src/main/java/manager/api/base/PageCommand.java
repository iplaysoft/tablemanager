package manager.api.base;

import lombok.Data;

@Data
public  class PageCommand {
    private long current = 0;
    private long size = 10;
    private boolean unlimited = false;
}
