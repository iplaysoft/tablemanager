package manager.api.templateitem;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yongfeng_meng
 */
@Data
public class TemplateItemUpdateCommand implements Serializable {

    private String fileName;

    private String name;

    private String manager;

    private String comment;

    private String content;

    private String type;

    private String language;
}
