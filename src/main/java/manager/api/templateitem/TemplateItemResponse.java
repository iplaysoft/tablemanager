package manager.api.templateitem;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import lombok.Data;

import java.io.Serializable;

@Data
public class TemplateItemResponse implements Serializable {
    private Long id;

    private Long templateId;

    private String fileName;

    private String name;

    private String manager;

    private String comment;

    private String content;

    private String type;

    private String language;

    private String createTime;

    private String updateTime;
}
