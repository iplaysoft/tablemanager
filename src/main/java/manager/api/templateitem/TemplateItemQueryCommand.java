package manager.api.templateitem;

import lombok.Data;
import manager.api.base.PageCommand;

import java.io.Serializable;

@Data
public class TemplateItemQueryCommand extends PageCommand implements Serializable {

    private Long templateId;

    private String nameLike;

    private String manager;
}
