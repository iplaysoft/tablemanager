package manager.api.templateitem;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yongfeng_meng
 */
@Data
public class TemplateItemCreateCommand implements Serializable {
    private Long templateId;

    private String fileName;

    private String name;

    private String manager;

    private String comment;

    private String content;

    private String type;

    private String language;
}
