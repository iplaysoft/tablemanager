package manager.api.entityitem;

import lombok.Data;

import java.io.Serializable;

@Data
public class EntityItemResponse implements Serializable {

    private Long id;

    private String code;

    private String name;

    private String comment;

    private String entityId;

    private String dataType;

    private Integer length;

    private Integer canSelect;

    private Integer canUpdate;

    private Integer canCondition;

    private String createTime;

    private String updateTime;
}
