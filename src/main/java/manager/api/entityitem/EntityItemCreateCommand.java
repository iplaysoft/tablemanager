package manager.api.entityitem;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Tony
 */
@Data
public class EntityItemCreateCommand implements Serializable {

    private Long entityId;

    private String code;

    private String name;

    private String comment;

    private String dataType;

    private Integer length;

    private Integer canSelect;

    private Integer canUpdate;

    private Integer canCondition;
}
