package manager.api.entityitem;

import lombok.Data;
import manager.api.base.PageCommand;

import java.io.Serializable;

@Data
public class EntityItemQueryCommand extends PageCommand implements Serializable {

    private Long entityId;

    private String code;

    private String nameLike;

    private String dataType;
}
