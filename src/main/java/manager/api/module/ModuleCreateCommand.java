package manager.api.module;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Tony
 */
@Data
public class ModuleCreateCommand implements Serializable {
    private Long projectId;
    private String code;
    private String name;
    private String manager;
    private String comment;
}
