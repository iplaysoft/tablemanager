package manager.api.module;

import lombok.Data;
import manager.api.base.PageCommand;

import java.io.Serializable;

@Data
public class ModuleQueryCommand extends PageCommand implements Serializable {

    private Long projectId;

    private String code;

    private String nameLike;

    private String manager;
}
