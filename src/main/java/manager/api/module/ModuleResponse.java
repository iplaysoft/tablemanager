package manager.api.module;

import lombok.Data;

import java.io.Serializable;

@Data
public class ModuleResponse implements Serializable {
    private Long id;

    private Long projectId;

    private String code;

    private String name;

    private String comment;

    private String manager;

    private String createTime;

    private String updateTime;
}
