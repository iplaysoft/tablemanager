package manager.api.module;

import lombok.Data;

import java.io.Serializable;

@Data
public class ModuleUpdateCommand implements Serializable {
    private String code;

    private String name;

    private String comment;

    private String manager;
}
