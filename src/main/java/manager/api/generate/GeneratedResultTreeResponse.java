package manager.api.generate;

import lombok.Data;
import manager.genarator.GeneratedResult;

import java.util.List;

@Data
public class GeneratedResultTreeResponse extends GeneratedResult {
    private boolean isDirectory;
    private List<GeneratedResultTreeResponse> children;
}
