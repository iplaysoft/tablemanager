package manager.api.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * @author Tony
 */
@Data
public class EntityCreateCommand implements Serializable {
    private Long moduleId;
    private String code;
    private String name;
    private String comment;
    private String manager;
}
