package manager.api.entity;

import lombok.Data;

/**
 * @author yongfeng_meng
 */
@Data
public class EntityImportCommand {
    private String type;

}
