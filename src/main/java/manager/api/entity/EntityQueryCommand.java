package manager.api.entity;

import lombok.Data;
import manager.api.base.PageCommand;

import java.io.Serializable;

@Data
public class EntityQueryCommand extends PageCommand implements Serializable {

    private Long moduleId;

    private String code;

    private String nameLike;

    private String manager;
}
