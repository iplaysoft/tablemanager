package manager.api.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class EntityUpdateCommand implements Serializable {

    private String code;

    private String name;

    private String comment;

    private String manager;
}
