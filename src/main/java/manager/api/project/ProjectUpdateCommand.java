package manager.api.project;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectUpdateCommand implements Serializable {

    private String code;

    private String name;

    private String comment;

    private String manager;
}
