package manager.api.project;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yongfeng_meng
 */
@Data
public class ProjectCreateCommand implements Serializable {
    private String code;

    private String name;

    private String manager;

    private String comment;
}
