package manager.api.project;

import lombok.Data;
import manager.api.base.PageCommand;

import java.io.Serializable;

@Data
public class ProjectQueryCommand extends PageCommand implements Serializable {
    private String code;

    private String nameLike;

    private String manager;
}
