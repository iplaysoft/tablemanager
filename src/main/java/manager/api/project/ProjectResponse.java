package manager.api.project;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectResponse implements Serializable {
    private Long id;

    private String code;

    private String name;

    private String comment;

    private String manager;

    private String createTime;

    private String updateTime;
}
