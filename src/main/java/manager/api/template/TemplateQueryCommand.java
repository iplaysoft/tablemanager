package manager.api.template;

import lombok.Data;
import manager.api.base.PageCommand;

import java.io.Serializable;

/**
 * @author yongfeng_meng
 */
@Data
public class TemplateQueryCommand extends PageCommand implements Serializable {
    private String code;

    private String nameLike;

    private String manager;
}
