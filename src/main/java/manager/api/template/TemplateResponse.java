package manager.api.template;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yongfeng_meng
 */
@Data
public class TemplateResponse implements Serializable {
    private Long id;

    private String code;

    private String name;

    private String comment;

    private String manager;

    private String createTime;

    private String updateTime;
}
