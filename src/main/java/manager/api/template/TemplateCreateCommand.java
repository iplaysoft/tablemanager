package manager.api.template;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yongfeng_meng
 */
@Data
public class TemplateCreateCommand implements Serializable {
    private String code;

    private String name;

    private String manager;

    private String comment;
}
