package manager.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import manager.domain.Module;
import manager.mapper.ModuleMapper;
import manager.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModuleService {
    @Autowired
    private ModuleMapper moduleMapper;

    public Long create(Module module) {
        module.setCreateTime(TimeUtil.now());
        moduleMapper.insert(module);
        return module.getId();
    }

    public IPage<Module> queryPage(Module module, long current, long size, boolean unlimited) {
        if (unlimited) {
            IPage<Module> page = new Page<>();
            List<Module> moduleList = moduleMapper.selectList(Wrappers.lambdaQuery(module));
            page.setRecords(moduleList);
            page.setSize(moduleList.size());
            return page;
        }
        return moduleMapper.selectPage(new Page<>(current, size), Wrappers.lambdaQuery(module));
    }

    public List<Module> query(QueryWrapper<Module> moduleQueryWrapper) {
        return moduleMapper.selectList(moduleQueryWrapper);
    }

    public Module get(Long id) {
        return moduleMapper.selectById(id);
    }

    public Module update(Module module) {
        module.setUpdateTime(TimeUtil.now());
        moduleMapper.updateById(module);
        return module;
    }

    public void delete(Long id) {
        moduleMapper.deleteById(id);
    }
}
