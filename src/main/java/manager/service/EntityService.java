package manager.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import manager.domain.Entity;
import manager.mapper.EntityMapper;
import manager.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class EntityService {
    @Autowired
    private EntityMapper entityMapper;

    public Long create(Entity entity) {
        entity.setCreateTime(TimeUtil.now());
        entityMapper.insert(entity);
        return entity.getId();
    }

    public IPage<Entity> queryPage(Entity entity, long current, long size, boolean unlimited) {
        if (unlimited) {
            IPage<Entity> page = new Page<>();
            List<Entity> entityList = entityMapper.selectList(Wrappers.lambdaQuery(entity));
            page.setRecords(entityList);
            page.setSize(entityList.size());
            return page;
        }
        return entityMapper.selectPage(new Page<>(current, size), Wrappers.lambdaQuery(entity));
    }

    public List<Entity> query(QueryWrapper<Entity> entityQueryWrapper) {
        return entityMapper.selectList(entityQueryWrapper);
    }

    public Entity get(Long id) {
        Entity entity = entityMapper.selectById(id);
        if (entity == null) {
            log.warn("can't find entity which id is {}", id);
        }
        return entity;
    }

    public Entity update(Entity entity) {
        entity.setUpdateTime(TimeUtil.now());
        entityMapper.updateById(entity);
        return entity;
    }

    public void delete(Integer id) {
        entityMapper.deleteById(id);
    }
}
