package manager.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import manager.domain.EntityItem;
import manager.mapper.EntityItemMapper;
import manager.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Consumer;

@Service
public class EntityItemService {
    @Autowired
    private EntityItemMapper entityItemMapper;

    public Long create(EntityItem entityItem) {
        entityItem.setCreateTime(TimeUtil.now());
        entityItemMapper.insert(entityItem);
        return entityItem.getId();
    }

    public IPage<EntityItem> queryPage(EntityItem entityItem, long current, long size, boolean unlimited) {
        if (unlimited) {
            IPage<EntityItem> page = new Page<>();
            List<EntityItem> entityItemList = entityItemMapper.selectList(Wrappers.lambdaQuery(entityItem));
            page.setRecords(entityItemList);
            page.setSize(entityItemList.size());
            return page;
        }
        return entityItemMapper.selectPage(new Page<>(current, size), Wrappers.lambdaQuery(entityItem));
    }

    public List<EntityItem> query(Consumer<LambdaQueryWrapper<EntityItem>> queryConsumer) {
        LambdaQueryWrapper<EntityItem> lambdaQueryWrapper = Wrappers.lambdaQuery();
        queryConsumer.accept(lambdaQueryWrapper);
        return entityItemMapper.selectList(lambdaQueryWrapper);
    }

    public Object get(Long id) {
        return entityItemMapper.selectById(id);
    }

    public EntityItem update(EntityItem entityItem) {
        entityItem.setUpdateTime(TimeUtil.now());
        entityItemMapper.updateById(entityItem);
        return entityItem;
    }

    public void delete(Integer id) {
        entityItemMapper.deleteById(id);
    }
}
