package manager.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import manager.domain.Template;
import manager.mapper.TemplateMapper;
import manager.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author yongfeng_meng
 */
@Service
public class TemplateService {
    @Autowired
    private TemplateMapper templateMapper;

    public Long create(Template template) {
        template.setCreateTime(TimeUtil.now());
        templateMapper.insert(template);
        return template.getId();
    }

    public IPage<Template> queryPage(Template template, long current, long size, boolean unlimited) {
        if (unlimited) {
            IPage<Template> page = new Page<>();
            List<Template> templategroupList = templateMapper.selectList(Wrappers.lambdaQuery(template));
            page.setRecords(templategroupList);
            page.setSize(templategroupList.size());
            return page;
        }
        return templateMapper.selectPage(new Page<>(current, size), Wrappers.lambdaQuery(template));
    }

    public List<Template> query(QueryWrapper<Template> templateQueryWrapper) {
        return templateMapper.selectList(templateQueryWrapper);
    }

    public Object get(Integer id) {
        return templateMapper.selectById(id);
    }

    public Template updateById(Long id, Template template) {
        template.setUpdateTime(TimeUtil.now());
        template.setId(id);
        if (templateMapper.updateById(template) > 0) {
            return templateMapper.selectById(id);
        }
        return null;
    }

    public void delete(Integer id) {
        templateMapper.deleteById(id);
    }
}
