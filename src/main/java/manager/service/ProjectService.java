package manager.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import manager.domain.Project;
import manager.mapper.ProjectMapper;
import manager.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectService {
    @Autowired
    private ProjectMapper projectMapper;

    public Long create(Project project) {
        project.setCreateTime(TimeUtil.now());
        projectMapper.insert(project);
        return project.getId();
    }

    public IPage<Project> queryPage(Project project, long current, long size, boolean unlimited) {
        if (unlimited) {
            IPage<Project> page = new Page<>();
            List<Project> projectList = projectMapper.selectList(Wrappers.lambdaQuery(project));
            page.setRecords(projectList);
            page.setSize(projectList.size());
            return page;
        }
        return projectMapper.selectPage(new Page<>(current, size), Wrappers.lambdaQuery(project));
    }

    public List<Project> query(QueryWrapper<Project> projectQueryWrapper) {
        return projectMapper.selectList(projectQueryWrapper);
    }

    public Project get(Long id) {
        return projectMapper.selectById(id);
    }

    public Project updateById(Long id, Project project) {
        project.setId(id);
        project.setUpdateTime(TimeUtil.now());
        if (projectMapper.updateById(project) > 0) {
            return projectMapper.selectById(id);
        }

        return null;
    }

    public void delete(Integer id) {
        projectMapper.deleteById(id);
    }
}
