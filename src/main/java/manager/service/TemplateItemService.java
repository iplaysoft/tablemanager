package manager.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import manager.domain.TemplateItem;
import manager.mapper.TemplateItemMapper;
import manager.util.TimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Consumer;

/**
 * @author yongfeng_meng
 */
@Service
public class TemplateItemService {
    @Autowired
    private TemplateItemMapper templateItemMapper;

    public Long create(TemplateItem templateItem) {
        templateItem.setCreateTime(TimeUtil.now());
        templateItemMapper.insert(templateItem);
        return templateItem.getId();
    }

    public IPage<TemplateItem> queryPage(TemplateItem templateItem, long current, long size, boolean unlimited) {
        if (unlimited) {
            IPage<TemplateItem> page = new Page<>();
            List<TemplateItem> templateItemList = templateItemMapper.selectList(Wrappers.lambdaQuery(templateItem));
            page.setRecords(templateItemList);
            page.setSize(templateItemList.size());
            return page;
        }
        return templateItemMapper.selectPage(new Page<>(current, size), Wrappers.lambdaQuery(templateItem));
    }

    public List<TemplateItem> query(Consumer<LambdaQueryWrapper<TemplateItem>> queryWrapper) {
        LambdaQueryWrapper<TemplateItem> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.accept(lambdaQueryWrapper);
        return templateItemMapper.selectList(lambdaQueryWrapper);
    }

    public Object get(Integer id) {
        return templateItemMapper.selectById(id);
    }

    public TemplateItem updateById(Long id, TemplateItem templateItem) {
        templateItem.setUpdateTime(TimeUtil.now());
        templateItem.setId(id);
        if (templateItemMapper.updateById(templateItem) > 0) {
            return templateItemMapper.selectById(id);
        }
        return null;
    }

    public void delete(Integer id) {
        templateItemMapper.deleteById(id);
    }
}
