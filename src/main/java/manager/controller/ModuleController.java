package manager.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ma.glasnost.orika.MapperFacade;
import manager.api.base.PageResponse;
import manager.api.module.ModuleCreateCommand;
import manager.api.module.ModuleQueryCommand;
import manager.api.module.ModuleResponse;
import manager.api.module.ModuleUpdateCommand;
import manager.domain.Module;
import manager.service.ModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Tony
 */
@RestController
@RequestMapping("module")
public class ModuleController {

    @Autowired
    private ModuleService moduleService;

    @Autowired
    private MapperFacade mapperFacade;

    @PostMapping
    public Long create(@RequestBody ModuleCreateCommand createCommand) {
        Module module = mapperFacade.map(createCommand, Module.class);
        return moduleService.create(module);
    }

    @GetMapping
    public PageResponse<ModuleResponse> queryPage(ModuleQueryCommand pageCommand) {
        Module module = mapperFacade.map(pageCommand, Module.class);
        IPage<Module> moduleIPage = moduleService.queryPage(module, pageCommand.getCurrent(), pageCommand.getSize(), pageCommand.isUnlimited());
        return new PageResponse<>(moduleIPage.getTotal(), mapperFacade.mapAsList(moduleIPage.getRecords(), ModuleResponse.class));
    }

    @GetMapping("{id}")
    public ModuleResponse get(@PathVariable Long id) {
        return mapperFacade.map(moduleService.get(id), ModuleResponse.class);
    }

    @PutMapping("{id}")
    public ModuleResponse update(@PathVariable Long id, @RequestBody ModuleUpdateCommand updateCommand) {
        Module module = mapperFacade.map(updateCommand, Module.class);
        module.setId(id);
        moduleService.update(module);
        return mapperFacade.map(module, ModuleResponse.class);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Long id) {
        moduleService.delete(id);
    }
}
