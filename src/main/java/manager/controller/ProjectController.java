package manager.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ma.glasnost.orika.MapperFacade;
import manager.api.base.PageResponse;
import manager.api.project.ProjectCreateCommand;
import manager.api.project.ProjectQueryCommand;
import manager.api.project.ProjectResponse;
import manager.api.project.ProjectUpdateCommand;
import manager.domain.Project;
import manager.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("project")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @Autowired
    private MapperFacade mapperFacade;

    @PostMapping
    public Long create(@RequestBody ProjectCreateCommand createCommand) {
        Project project = mapperFacade.map(createCommand, Project.class);
        return projectService.create(project);
    }

    @GetMapping
    public PageResponse<ProjectResponse> queryPage(ProjectQueryCommand pageCommand) {
        Project project = mapperFacade.map(pageCommand, Project.class);
        IPage<Project> projectIPage = projectService.queryPage(project, pageCommand.getCurrent(), pageCommand.getSize(), pageCommand.isUnlimited());
        return new PageResponse<>(projectIPage.getTotal(), mapperFacade.mapAsList(projectIPage.getRecords(), ProjectResponse.class));
    }

    @GetMapping("{id}")
    public ProjectResponse get(@PathVariable Long id) {
        return mapperFacade.map(projectService.get(id), ProjectResponse.class);
    }

    @PutMapping("{id}")
    public ProjectResponse update(@PathVariable Long id, @RequestBody ProjectUpdateCommand updateCommand) {
        Project project = mapperFacade.map(updateCommand, Project.class);
        return mapperFacade.map(projectService.updateById(id, project), ProjectResponse.class);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Integer id) {
        projectService.delete(id);
    }
}
