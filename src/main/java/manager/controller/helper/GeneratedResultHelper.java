package manager.controller.helper;

import manager.api.generate.GeneratedResultTreeResponse;
import manager.genarator.GeneratedResult;
import manager.util.FileUtil;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class GeneratedResultHelper {
    public static List<GeneratedResultTreeResponse> convertToTree(List<GeneratedResult> generatedResults) {
        Map<String, List<GeneratedResult>> pathGroup =
                generatedResults.stream().collect(Collectors.groupingBy(
                        generatedResult -> FileUtil.getDirectory(generatedResult.getFileName()),
                        TreeMap::new, Collectors.toList()));

        return pathGroup.entrySet().stream().map(stringListEntry -> {
            List<GeneratedResultTreeResponse> files = stringListEntry.getValue().stream().map(generatedResult -> {
                GeneratedResultTreeResponse file = new GeneratedResultTreeResponse();
                file.setDirectory(false);
                file.setFileName(FileUtil.getFileName(generatedResult.getFileName()));
                file.setLanguage(generatedResult.getLanguage());
                file.setContent(generatedResult.getContent());
                return file;
            }).collect(Collectors.toList());

            GeneratedResultTreeResponse directory = new GeneratedResultTreeResponse();
            directory.setDirectory(true);
            directory.setFileName(stringListEntry.getKey());
            directory.setChildren(files);
            return directory;
        }).collect(Collectors.toList());
    }
}
