package manager.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.extern.slf4j.Slf4j;
import ma.glasnost.orika.MapperFacade;
import manager.api.base.PageResponse;
import manager.api.entity.*;
import manager.api.generate.GeneratedResultTreeResponse;
import manager.controller.helper.GeneratedResultHelper;
import manager.domain.*;
import manager.genarator.GenerateContext;
import manager.genarator.GeneratedResult;
import manager.genarator.Generator;
import manager.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author yongfeng_meng
 */
@RestController
@RequestMapping("entity")
@Slf4j
public class EntityController {

    @Autowired
    private EntityService entityService;

    @Autowired
    private EntityItemService entityItemService;

    @Autowired
    private ModuleService moduleService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private TemplateItemService templateItemService;

    @Autowired
    private MapperFacade mapperFacade;

    @Autowired
    private Map<String, Generator> generatorMap;

    @GetMapping("{id}/generated-result")
    public List<GeneratedResult> generator(@PathVariable Long id, @RequestParam Long[] templateItemIds) {
        Entity entity = entityService.get(id);
        if (entity == null) {
            return Collections.EMPTY_LIST;
        }
        List<EntityItem> entityItems = entityItemService.query(wrapper -> wrapper.eq(EntityItem::getEntityId, entity.getId()));
        if (entityItems.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        Module module = moduleService.get(entity.getModuleId());
        if (module == null) {
            return Collections.EMPTY_LIST;
        }
        Project project = projectService.get(module.getProjectId());
        if (project == null) {
            return Collections.EMPTY_LIST;
        }
        GenerateContext generateContext = new GenerateContext();
        generateContext.setEntity(entity);
        generateContext.setModule(module);
        generateContext.setProject(project);
        generateContext.setEntityItems(entityItems);

        List<TemplateItem> templateItems = templateItemService.query(wrapper -> wrapper.in(TemplateItem::getId, templateItemIds));

        List<GeneratedResult> generatedResults = new ArrayList<>(templateItemIds.length);
        for (TemplateItem templateItem : templateItems) {
            Generator generator = generatorMap.get(templateItem.getType());
            if (generator != null) {
                generateContext.setTemplateItem(templateItem);
                generatedResults.add(generator.generate(generateContext));
            }
        }
        return generatedResults;
    }

    @GetMapping("{id}/generated-result-tree")
    public List<GeneratedResultTreeResponse> generatorTree(@PathVariable Long id, @RequestParam Long[] templateItemIds) {
        return GeneratedResultHelper.convertToTree(generator(id, templateItemIds));
    }

    @PostMapping
    public Long create(@RequestBody EntityCreateCommand createCommand) {
        Entity entity = mapperFacade.map(createCommand, Entity.class);
        return entityService.create(entity);
    }

    @GetMapping
    public PageResponse<EntityResponse> queryPage(EntityQueryCommand pageCommand) {
        Entity entity = mapperFacade.map(pageCommand, Entity.class);
        IPage<Entity> entityIPage = entityService.queryPage(entity, pageCommand.getCurrent(), pageCommand.getSize(), pageCommand.isUnlimited());
        return new PageResponse<>(entityIPage.getTotal(), mapperFacade.mapAsList(entityIPage.getRecords(), EntityResponse.class));
    }

    @GetMapping("{id}")
    public EntityResponse get(@PathVariable Long id) {
        return mapperFacade.map(entityService.get(id), EntityResponse.class);
    }

    @PutMapping("{id}")
    public EntityResponse update(@PathVariable Long id, @RequestBody EntityUpdateCommand updateCommand) {
        Entity entity = mapperFacade.map(updateCommand, Entity.class);
        entity.setId(id);
        entityService.update(entity);
        return mapperFacade.map(entity, EntityResponse.class);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Integer id) {
        entityService.delete(id);
    }

    @PostMapping("import")
    public void importEntity(EntityImportCommand command) {
    }
}
