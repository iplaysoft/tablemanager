package manager.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ma.glasnost.orika.MapperFacade;
import manager.api.base.PageResponse;
import manager.api.entityitem.EntityItemCreateCommand;
import manager.api.entityitem.EntityItemQueryCommand;
import manager.api.entityitem.EntityItemResponse;
import manager.api.entityitem.EntityItemUpdateCommand;
import manager.domain.EntityItem;
import manager.service.EntityItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Tony
 */
@RestController
@RequestMapping("entity-item")
public class EntityItemController {

    @Autowired
    private EntityItemService entityItemService;

    @Autowired
    private MapperFacade mapperFacade;

    @PostMapping
    public Long create(@RequestBody EntityItemCreateCommand createCommand) {
        EntityItem entityItem = mapperFacade.map(createCommand, EntityItem.class);
        return entityItemService.create(entityItem);
    }

    @GetMapping
    public PageResponse<EntityItemResponse> queryPage(EntityItemQueryCommand pageCommand) {
        EntityItem entityItem = mapperFacade.map(pageCommand, EntityItem.class);
        IPage<EntityItem> itemIPage = entityItemService.queryPage(entityItem, pageCommand.getCurrent(), pageCommand.getSize(), pageCommand.isUnlimited());
        return new PageResponse<>(itemIPage.getTotal(), mapperFacade.mapAsList(itemIPage.getRecords(), EntityItemResponse.class));
    }

    @GetMapping("{id}")
    public EntityItemResponse get(@PathVariable Long id) {
        return mapperFacade.map(entityItemService.get(id), EntityItemResponse.class);
    }

    @PutMapping("{id}")
    public EntityItemResponse update(@PathVariable Long id, @RequestBody EntityItemUpdateCommand updateCommand) {
        EntityItem entityItem = mapperFacade.map(updateCommand, EntityItem.class);
        entityItem.setId(id);
        entityItemService.update(entityItem);
        return mapperFacade.map(entityItem, EntityItemResponse.class);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Integer id) {
        entityItemService.delete(id);
    }
}
