package manager.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ma.glasnost.orika.MapperFacade;
import manager.api.base.PageResponse;
import manager.api.templateitem.TemplateItemCreateCommand;
import manager.api.templateitem.TemplateItemQueryCommand;
import manager.api.templateitem.TemplateItemResponse;
import manager.api.templateitem.TemplateItemUpdateCommand;
import manager.domain.TemplateItem;
import manager.service.TemplateItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author yongfeng_meng
 */
@RestController
@RequestMapping("template-item")
public class TemplateItemController {

    @Autowired
    private TemplateItemService templateItemService;

    @Autowired
    private MapperFacade mapperFacade;

    @PostMapping
    public Long create(@RequestBody TemplateItemCreateCommand createCommand) {
        TemplateItem templateItem = mapperFacade.map(createCommand, TemplateItem.class);
        return templateItemService.create(templateItem);
    }

    @GetMapping
    public PageResponse<TemplateItemResponse> queryPage(TemplateItemQueryCommand pageCommand) {
        TemplateItem templateItem = mapperFacade.map(pageCommand, TemplateItem.class);
        IPage<TemplateItem> templateItemIPage = templateItemService.queryPage(templateItem, pageCommand.getCurrent(), pageCommand.getSize(), pageCommand.isUnlimited());
        return new PageResponse<>(templateItemIPage.getTotal(), mapperFacade.mapAsList(templateItemIPage.getRecords(), TemplateItemResponse.class));
    }

    @GetMapping("{id}")
    public TemplateItemResponse get(@PathVariable Integer id) {
        return mapperFacade.map(templateItemService.get(id), TemplateItemResponse.class);
    }

    @PutMapping("{id}")
    public TemplateItemResponse update(@PathVariable Long id, @RequestBody TemplateItemUpdateCommand updateCommand) {
        TemplateItem templateItem = mapperFacade.map(updateCommand, TemplateItem.class);
        return mapperFacade.map(templateItemService.updateById(id, templateItem), TemplateItemResponse.class);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Integer id) {
        templateItemService.delete(id);
    }
}
