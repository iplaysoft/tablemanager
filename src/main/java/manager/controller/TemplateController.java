package manager.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import ma.glasnost.orika.MapperFacade;
import manager.api.base.PageResponse;
import manager.api.template.TemplateCreateCommand;
import manager.api.template.TemplateQueryCommand;
import manager.api.template.TemplateResponse;
import manager.api.template.TemplateUpdateCommand;
import manager.domain.Template;
import manager.service.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author yongfeng_meng
 */
@RestController
@RequestMapping("template")
public class TemplateController {

    @Autowired
    private TemplateService templateService;

    @Autowired
    private MapperFacade mapperFacade;

    @PostMapping
    public Long create(@RequestBody TemplateCreateCommand createCommand) {
        Template template = mapperFacade.map(createCommand, Template.class);
        return templateService.create(template);
    }

    @GetMapping
    public PageResponse<TemplateResponse> queryPage(TemplateQueryCommand pageCommand) {
        Template template = mapperFacade.map(pageCommand, Template.class);
        IPage<Template> templateGroupIPage = templateService.queryPage(template, pageCommand.getCurrent(), pageCommand.getSize(), pageCommand.isUnlimited());
        return new PageResponse<>(templateGroupIPage.getTotal(), mapperFacade.mapAsList(templateGroupIPage.getRecords(), TemplateResponse.class));
    }

    @GetMapping("{id}")
    public TemplateResponse get(@PathVariable Integer id) {
        return mapperFacade.map(templateService.get(id), TemplateResponse.class);
    }

    @PutMapping("{id}")
    public TemplateResponse update(@PathVariable Long id, @RequestBody TemplateUpdateCommand updateCommand) {
        Template template = mapperFacade.map(updateCommand, Template.class);
        return mapperFacade.map(templateService.updateById(id, template), TemplateResponse.class);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Integer id) {
        templateService.delete(id);
    }
}
