package manager.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import manager.pojo.ModulePOJO;
import manager.sqlite.SqlCondition;

@Data
public class Module extends ModulePOJO {

    @TableField(value = "name", select = false, condition = SqlCondition.LIKE)
    private String nameLike;
}
