package manager.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import manager.pojo.TemplatePOJO;
import manager.sqlite.SqlCondition;

/**
 * @author Tony
 */
@Data
public class Template extends TemplatePOJO {

    @TableField(value = "name", select = false, condition = SqlCondition.LIKE)
    private String nameLike;
}
