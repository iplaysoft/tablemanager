package manager.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import manager.pojo.TemplateItemPOJO;
import manager.sqlite.SqlCondition;

/**
 * @author Tony
 */
@Data
public class TemplateItem extends TemplateItemPOJO {

    @TableField(value = "name", select = false, condition = SqlCondition.LIKE)
    private String nameLike;
}
