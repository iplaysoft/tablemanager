package manager.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import manager.pojo.ProjectPOJO;
import manager.sqlite.SqlCondition;

/**
 * @author Tony
 */
@Data
public class Project extends ProjectPOJO {

    @TableField(value = "name", select = false, condition = SqlCondition.LIKE)
    private String nameLike;
}
