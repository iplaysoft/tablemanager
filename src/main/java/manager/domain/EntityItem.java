package manager.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import manager.pojo.EntityItemPOJO;
import manager.sqlite.SqlCondition;

/**
 * @author yongfeng_meng
 */
@Data
public class EntityItem extends EntityItemPOJO {

    @TableField(value = "name", select = false, condition = SqlCondition.LIKE)
    private String nameLike;
}
