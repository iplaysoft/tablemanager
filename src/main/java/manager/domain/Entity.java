package manager.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import manager.pojo.EntityPOJO;
import manager.sqlite.SqlCondition;

public class Entity extends EntityPOJO {

    @TableField(value = "name", select = false, condition = SqlCondition.LIKE)
    private String nameLike;
}
